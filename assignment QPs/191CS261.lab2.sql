create database hospital_managment_system;
use hospital_managment_system;
create table DOCTORS(
	d_id int not null,
    d_name varchar(15) not null,
    salary int not null,
    specification varchar(15) not null,
    primary key (d_id)
);
create table ROOM(
	r_id int not null,
    room_type varchar(10) not null,
    primary key (r_id) 
);
create table PATIENT(
	p_id int not null,
    r_id int not null,
    d_id int not null,
    p_name varchar(20) not null,
    city varchar(15) not null,
    contact numeric(10),
    primary key(p_id),
    foreign key (r_id) references ROOM(r_id),
    foreign key (d_id) references DOCTORS(d_id)
);
create table TESTandDIAGNOSIS (
	p_id int not null,
    diagno varchar(15),
    diagdetails varchar(15),
    foreign key (p_id) references PATIENT(p_id)
);

describe DOCTORS;
describe ROOM;
describe PATIENT;
describe TESTandDIAGNOSIS;

insert into ROOM values(1,"OPERATION");
insert into ROOM values(2,"CHILDREN");
insert into ROOM values(3,"OLD");
insert into ROOM values(4,"LAB");
insert into ROOM values(5,"GENERAL");

insert into DOCTORS VALUES(1,"VAMSHI",10000,"HEART");
insert into DOCTORS VALUES(2,"VAM",10000,"KIDNEY");
insert into DOCTORS VALUES(3,"KRISHNA",50000,"BRAIN");
insert into DOCTORS VALUES(4,"RAM",1000,"LIVER");
insert into DOCTORS VALUES(5,"HARI",10000,"EYES");


insert into PATIENT values(1,1,1,"VAMSHIKRISHNA","DELHI",9874563210);
insert into PATIENT values(2,2,2,"RAKESH","KOLKATTA",1236547896);
insert into PATIENT values(3,3,3,"SHARMA","BANGALORE",3214567890);
insert into PATIENT values(4,4,4,"GANDHI","DELHI",7894565230);
insert into PATIENT values(5,5,5,"HARI","MANGALORE",0123456987);

select * from DOCTORS;
select* from PATIENT;
select* FROM ROOM;
select * FROM TESTandDIAGNOSIS;


insert into TESTandDIAGNOSIS values(1,"AB2","TESTING");
insert into TESTandDIAGNOSIS values(2,"AB1","CARDOLOGY");
insert into TESTandDIAGNOSIS values(3,"AB3","KIDNEY");
insert into TESTandDIAGNOSIS values(4,"AB4","CHECKUP");
insert into TESTandDIAGNOSIS(p_id,diagno) valueS(5,"AB5");
select PATIENT.p_id , PATIENT.p_name , PATIENT.city , PATIENT.contact ,ROOM.r_id, ROOM.room_type,
 DOCTORS.d_id , DOCTORS.d_name , DOCTORS.salary , DOCTORS.specification , TESTandDIAGNOSIS.diagno,TESTandDIAGNOSIS.diagdetails
from PATIENT,ROOM,DOCTORS,TESTandDIAGNOSIS where ROOM.r_id=PATIENT.r_id and DOCTORS.d_id=PATIENT.d_id and TESTandDIAGNOSIS.p_id=PATIENT.p_id;

drop database hospital_managment_system;
