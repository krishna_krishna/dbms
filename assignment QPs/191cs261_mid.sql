create database Birdwatcher;
use Birdwatcher;
create table BIRDS(
b_id int not null,
species varchar(15) not null,
legband varchar(15) null,
primary key(b_id),
unique(legband)
);
create table OBSERVERS(
o_id int not null,
name varchar(15) not null,
primary key(o_id)
);
create table SIGHTINGS(
o_id int not null,
b_id int not null,
w_date date not null,
latitude float not null,
longitude float not null,
foreign key (b_id) references BIRDS(b_id),
foreign key (o_id) references OBSERVERS(o_id)
);
/*alter table SIGHTINGS  change latitude  latitude float; 
alter table SIGHTINGS  change longitude  longitude float;*/ 
-- drop database Birdwatcher;
insert into  BIRDS values(1,"Myna","MYLB");
insert into  BIRDS values(2,"Myna","MYRB");
insert into  BIRDS values(3,"Raven","MROB");
insert into  BIRDS values(4,"Raven","MRLB");
insert into  BIRDS values(5,"parakeet","MPSD");
insert into  BIRDS values(6,"Kingfisher","MKGH");
insert into  BIRDS values(7,"BlueJay","MBSY");

insert into OBSERVERS values(1,"Anand");
insert into OBSERVERS values(2,"Ankit");
insert into OBSERVERS values(3,"Alice");
insert into OBSERVERS values(4,"Bob");
insert into OBSERVERS values(5,"Bhuvan");
insert into OBSERVERS values(6,"Carol");
insert into OBSERVERS values(7,"Dinesh");
insert into OBSERVERS values(8,"Santhosh");

insert into SIGHTINGS values(3,2,"2014-03-01",43.12,-76.23);
insert into SIGHTINGS values(2,1,"2020-01-19",41.34,-77.29);
insert into SIGHTINGS values(1,2,"2016-05-23",42.23,-76.56);
insert into SIGHTINGS values(4,3,"2015-03-22",44.17,-77.21);
insert into SIGHTINGS values(6,4,"2017-08-17",43.23,-75.34);
insert into SIGHTINGS values(5,5,"2020-09-20",44.12,-77.22);
insert into SIGHTINGS values(7,6,"2018-08-12",47.34,-78.12);
insert into SIGHTINGS values(8,7,"2019-09-18",44.12,-75.67);
insert into SIGHTINGS values(2,5,"2020-08-12",46.23,-75.46);
insert into SIGHTINGS values(3,4,"2017-09-05",43.25,-76.27);
