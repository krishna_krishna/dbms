-- create database hospital_managment_system;
use hospital_managment_system;
/*create table DOCTORS(
	d_id int not null,
    d_name varchar(15) not null,
    salary int not null,
    specification varchar(15) not null,
    primary key (d_id)
);
create table ROOM(
	r_id int not null,
    room_type varchar(10) not null,
    primary key (r_id) 
);
create table PATIENT(
	p_id int not null,
    r_id int not null,
    d_id int not null,
    p_name varchar(20) not null,
    city varchar(15) not null,
    contact numeric(10),
    primary key(p_id),
    foreign key (r_id) references ROOM(r_id),
    foreign key (d_id) references DOCTORS(d_id)
);
create table TESTandDIAGNOSIS (
	p_id int not null,
    diagno varchar(15),
    diagdetails varchar(15),
    foreign key (p_id) references PATIENT(p_id)
);*/

describe DOCTORS;
describe ROOM;
describe PATIENT;
describe TESTandDIAGNOSIS;

alter table PATIENT add age int null;
alter table DOCTORS add age int null;
describe PATIENT;
describe DOCTORS;

insert into ROOM values(6,"AC");
insert into ROOM values(7,"COOLED");
insert into ROOM values(8,"OLD");
insert into ROOM values(9,"LAB");
insert into ROOM values(10,"GENERAL");

insert into DOCTORS VALUES(6,"abcds",10000,"HEART",52);
insert into DOCTORS VALUES(7,"bcdh",10000,"KIDNEY",20);
insert into DOCTORS VALUES(8,"kbch",50000,"BRAIN",25);
insert into DOCTORS VALUES(9,"ka",10000,"LIVER",60);
insert into DOCTORS VALUES(10,"abcd",10000,"EYES",30);


insert into PATIENT values(6,6,6,"VAMSHIKRISHNA","Calcutta",9874563210,10);
insert into PATIENT values(7,7,7,"RAKESH","chennai",1236547896,20);
insert into PATIENT values(8,8,8,"SHARMA","Bangalore",3214567890,100);
insert into PATIENT values(9,9,9,"GANDHI","DELHI",7894565230,50);
insert into PATIENT values(10,10,10,"RAKESH","MANGALORE",0123456987,35);

insert into TESTandDIAGNOSIS values(5,"AB2","TESTING");
insert into TESTandDIAGNOSIS values(6,"AB1","CARDOLOGY");
insert into TESTandDIAGNOSIS values(7,"AB3","KIDNEY");
insert into TESTandDIAGNOSIS values(9,"AB4","CHECKUP");
insert into TESTandDIAGNOSIS values(10,"AB4","CHECKUP");

select* from DOCTORS;
select * FROM PATIENT;
select * FROM ROOM;
select * FROM TESTandDIAGNOSIS;


select specification from DOCTORS where d_name like 'a%';
select specification from DOCTORS where d_name like '%h';
select specification from DOCTORS where d_name like 'a%s';
select specification from DOCTORS where d_name like 'ab%';
select specification from DOCTORS where d_name like '__c%';
select specification from DOCTORS where d_name like '%k___%';

select p_name from PATIENT WHERE age>(select  distinct max(DOCTORS.age) from DOCTORS);

select p_name from PATIENT where city="calcutta" or city="chennai";

select * from DOCTORS WHERE salary>25000;

SELECT DISTINCT(d_name)FROM DOCTORS WHERE DOCTORS.d_id NOT IN (SELECT d_id FROM PATIENT);

update DOCTORS set d_name="mishra" where d_id=1;
select* from DOCTORS;


SELECT d_name,SUM(salary) AS total_salary FROM DOCTORS GROUP BY d_name;

select distinct(p_name) from PATIENT;

select * from PATIENT where city!="Delhi";

select * from PATIENT where city!="Delhi"and city!="Bangalore";


select * from PATIENT WHERE (select p_id from TESTandDIAGNOSIS where diagno between 2 and 5);

select room_type from ROOM where (select p_id from PATIENT where P_id=305);

select * from PATIENT where age=25 and age=29 and age=39 and age=49;








