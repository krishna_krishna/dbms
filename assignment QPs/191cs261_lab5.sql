use hospital_managment_system;

 
 insert into ROOM values(101,"deluxe",500);
 insert into ROOM values(102,"op",2000);
 insert into ROOM values(103,"deluxe",5000);
 insert into ROOM values(104,"a/c",50000);
 insert into ROOM values(105,"non-a/c",5200);
 
 
insert into DOCTORS values("11","sunadar",50000,"heart",25);
insert into DOCTORS values("12","a.v",4000,"kidney",50);
insert into DOCTORS values("13","basavaraj",50000,"liver",60);
insert into DOCTORS values("14","pavan",10000,"heart",45);
insert into DOCTORS values("15","rakesh",60000,"eye",40);

insert into TESTandDIAGNOSIS values(11,"AB2","CHECKUP");
insert into TESTandDIAGNOSIS values(12,"AB3","testing");
insert into TESTandDIAGNOSIS values(13,"AB2","BRAIN");
insert into TESTandDIAGNOSIS values(14,"AB3","HEART CHECKUP");
insert into TESTandDIAGNOSIS values(15,"AB4","CHECKUP");


INSERT into PATIENT VALUES(11,101,11,"JAKES","KOPPAL",1234567890,25,"2016-12-12");
INSERT into PATIENT VALUES(12,102,12,"CAKES","BALLARY",1597534680,52,"2018-12-12");
INSERT into PATIENT VALUES(13,103,13,"ARON","VIJAYANAGARA",7563214569,25,"2018-12-12");
INSERT into PATIENT VALUES(14,104,14,"SHYAM ","CHAMRAJANAGARA",01234567850,85,"2019-12-12");
INSERT into PATIENT VALUES(15,105,15,"DARSHAN","HASSAN",4567891230,28,"2018-12-12");

select distinct PATIENT.*
from PATIENT natural join TESTandDIAGNOSIS
where p_id in ( select p_id from TESTandDIAGNOSIS
				group by p_id having count(p_id) > 1);
                
alter table PATIENT ADD p_date date;
update PATIENT SET DATE="2016-12-12" WHERE p_id=1;
describe PATIENT;
select  * from PATIENT;


select d_id,name from DOCTORS  where d_id not in (select d.d_id from DOCTORS d join PATIENT p on p.d_id = d.d_id);


select salary from DOCTORS order by salary;

select * from PATIENT
where p_id in
(select p_id from TESTandDIAGNOSIS group by p_id having count(diagdetails) > 0 );

select d.d_id,d.name, count(d.d_id) as PATIENT
from DOCTORS d join PATIENT p on p.d_id = d.d_id
group by d.d_id
having count(d.d_id) >3;

select DOCTORS.d_id,name,p_id,p_name,r_id 
from DOCTORS join PATIENT on DOCTORS.d_id = PATIENT.d_id
where r_id between 102 and 105 ;


select p_date,p_id,p_name
from PATIENT
order by p_date;


select count(p_id) as patients_with_deluxe_rooms
from PATIENT
where r_id in (select r_id from ROOM where room_type = "deluxe");


select name,salary
from DOCTORS
where salary < 40000;


select p_id,p_name,p_date
from PATIENT
where p_date < '2017-10-10';